# GOSH-LISTS

This repository contains JSON files with relevant community information.
Two examples include: GOSH projects, GOSH residencies

## How to include your own information:
1. In this repository, switch to the "working" branch and then select the file you wish to EDIT.
![Working Branch](/uploads/e716e958ae2cae4cc01ff1b17e13fae0/Screenshot_from_2019-09-04_14-58-56.png)
2. Once you can see the file, click the "Edit" button.
![Edit](/uploads/66c55337c882afd5af1a6f0db7b500c9/Screenshot_from_2019-09-04_15-00-14.png)
3. Make changes to the file. Stick to the format that is already being used. If there are errors, the changes won't be approved. You can check that you formatted the new information correctly by going to [JSONLint](https://jsonlint.com/) to check the validity of the file.
![Make Changes](/uploads/d6cce18e10b13f9d69e9e80ff4cf9894/Screenshot_from_2019-09-04_15-01-19.png)
4. Commit the changes by pressing the button below.
5. Go to the "Merge Requests" bar on the left hand side. Click "New merge request".
![Merge Request](/uploads/b1bc6158caff417e4a3a6c0570db61b4/Screenshot_from_2019-09-04_15-02-28.png)
6. Select the "working" branch on the left and the "master" branch on the right. Click "Compare branches and continue".
![Source/Target Branch](/uploads/a3f0f49b7cde9d3685153552b6f5be8c/Screenshot_from_2019-09-04_15-02-57.png)
7. Scroll to the bottom and press "Submit merge request".
![Submit Merge Request](/uploads/7e18692a622a51c49d8fd7b0ed612bbc/Screenshot_from_2019-09-04_15-03-25.png)
8. Congratulations, your changes are being reviewed and might be accepted (or the maintainer will request that you fix any problems).
![Complete](/uploads/d842797a9aafec050f6a42e3c89b200a/Screenshot_from_2019-09-04_15-03-57.png)

